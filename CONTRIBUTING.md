### Contributing to critical-kits

*(This is taken from some git standard guides, adapted for our use)*

We love that you're interested in contributing to this project!

To make the process as painless as possible, we have just a couple of guidelines
that should make life easier for everyone involved.

### Issues:

The issue list is a place to track what needs to be done.  Creating and closing them is cheap, so make liberal use of them :-)

You can do this with any gitlab account, you don't need permission

It's easy to see which issues need addressing by looking at the [open issue list](https://gitlab.com/redock/critical-kits/issues) but there are other ways to slice and dice things.
Issues get [labelled](https://gitlab.com/redock/critical-kits/labels) as a way to group them together - feel free to create new labels as and when needed.  And there are a few [milestones](https://gitlab.com/redock/critical-kits/milestones) to mark when issues have to be finished.  
Use milestones for things that '''have''' to be done before a set event, rather than for "it'd be lovely if this was done by date X", as that just clutters things up if the deadline isn't hit.

For some more background on good practice when using issues, read http://ben.balter.com/2014/11/06/rules-of-communicating-at-github/.
It's about git/Github's culture and process rather than exactly how we're running things, but there's plenty of crossover.

You might like the boards view of the issues also
https://gitlab.com/redock/critical-kits/boards

### Markdown

Markdown is how you write and format text on github & gitlab. `html` tagging also works ;)

A good [guide can be found here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


### Contributing Code

Fork this repository, make it awesomer (preferably in a branch named for the topic), send a pull request!

### Prefer Pull Requests

If you know exactly how to implement the feature being suggested or fix the bug
being reported, please open a pull request instead of an issue. Pull requests are easier than
patches or inline code blocks for discussing and merging the changes.

If you can't make the change yourself, please open an issue after making sure
that one isn't already logged.