# Git and the Dark Matter of Communal Documentation

I've only 15 minutes to talk about this project but it's really the reason why I'm banging on about GitLab and making Re-Dock use it to organise this conference. I'm going to just talk about the project but I've included my own notes here and will refer to them. They are really some notes of a blog post I'm writing about working on [critical-kits](http://criticalkits.re-dock.org).

Much of this talk and the content of this symposium is informed by my experiences being based at [DoESLiverpool](http://doesliverpool.com) and an artist who works with people like [Re-Dock](http://re-dock.org), [Octopus Collective](http://www.octopuscollective.org/) and [FACT](http://fact.co.uk) and [Das Labor](https://www.das-labor.org) I'll also be roping in [Adrian McEwen](https://twitter.com/amcewen) on this.

The talk is mainly based on observations and experiences of using a variety of practices and tools like [GitHub](https://github.com) to develop and manage a complex DoESLiverpool project called [Desktop Prosthetics](https://github.com/cheapjack/buildyourown)

This project which is **not mine** by the way, in many ways articulated and challenged an ongoing interest in technical cultures especially around ideas of `open-source` `DIY` and touched on the more recent `Maker` meme (I won't say *movement* because it's well, not a movement really).

In a way presenting this project is difficult and time consuming to talk about. First off it's not mine, it was never set out as something that must be ethically open-sourced and it wasn't any artist's idea. Like many projects it just happened to work out like that. As the only artist contributing to the project (although Adrian is one these days) I actually only really facilitated and helped project manage and learn about 3d printing alongside the Abbott family, Adrian McEwen, Patrick Fenner and the DoES community.

Easy description would be a **Heart Warming Desktop 3D printing upper limb prosthetics project by Liverpool family & DoESLiverpool**. However what is easily lost in description like that was the contextual relationships extensive planning and narratives around the project.
 * The original family who came into the office one day to 3D print something, ending up learning how to do it themselves
 * Their suggestion of working with a local prosthetics charity REACH to build an interest group of families around the exhibition
 * Crafts Council and FACT inviting DoES to suggest some kind of maker exhibition at one point to make an open source combine harvester or something before getting super excited over the idea of Desktop Prosthetics
 * Connecting with Enabling the Future a global open source volunteer network
 * Finding local Enabling the Future volunteers
 * Training up John & Washington the FACT gallery managers
 * Develop some measuring, hacking and fitting activities and ongoing interest groups
 * Watching the post-exhibition interest group grow then eventually drift away, although we still have a prosthetics project box for anyone who wants to contribute.
 * Watch the fledgling Norwich Hackspace take on the project as it effectively 'toured' and use it to gather momentum to build a new Makerspace.

This project had many many partners and interest groups, became relatively ambitious and was a little under resourced: as such it generated an incredible amount of email and communication that became difficult to manage. We realised the only way we could survive this and still do the project and it's stakeholders justice was by directing people to a central location for information.

And because we were planning, finding, fixing and anticipating problems a fixed website wasn't going to cut it. We needed any question or issue that came our way to be directed to  a constantly updated `README.md` file on github and so we forced non-technical people, non-programmers (like me) to use git issues not because *we think people should* but because otherwise **it just wasn't going to happen**.  

## Not Mine

You can refer to the [Desktop Prosthetics](http://github.com/cheapjack/buildyourown) repo to examine the extent that this project is **not mine**. You can also dig around into the issues both open and closed and get a sense of that contextual stuff.

I think all of our work as artists that involves collaborating or even just telling people to do things is easily as complex as this. Often it's equally risky and involves not just project management skills but being able to communicate well and listen to people, and then act on what you hear.

And there is always so much hidden planning, self criticism and self policing not to mention research and learning as you go along.

When someone makes a nice video of the activity you and your partners developed it is nice but it's not capturing it's true nature especially if the end results of your project is not a big impressive installation but some wires hanging out of a breadboard gaffa-taped to a tabard or a half finished prosthetic that needs re-printing.

Often in participatory settings the shiny impressive OMG installation aesthetic is not always available, affordable or indeed, appropriate. So the fine grain of the project, why with whom and the how and when of it becomes all the more important. Even these projects are sold short by a simple beautifully shot HD video of children pointing or making a book with an essay by the curator with pictures of people drinking coffee, technical diagrams with no context or people sitting in a circle talking or soldering intently.

What's missing is the **Dark Matter**; the stuff that makes up nearly everything but somehow remains invisible and cannot be harnessed fully or easily understood. See Adrian's talk on the [Dark Matter Of Maker Spaces](https://doesliverpool.com/slides/future-makespaces-talk-the-dark-matter-of-makerspaces/) that he points people toward when they want an answer to "What is DoESLiverpool"

Tools that reveal this Dark Matter perhaps are needed: and git may not be the right tool for this but it's an interesting model and a starting point as it can tell a very fine grained story.

Somewhere between git, hand made board games and [text adventures](http://domesticscience.org.uk/games.html) and what myself, **Glenn Boulter** and **Hwa Young** have been calling **Interactive Non-Fiction** where a good part of the artwork is the [documentation of itself](drinkme.textadventuretime.co.uk).

Find [cheapjack on GitHub](https://github.com/cheapjack?tab=repositories)
