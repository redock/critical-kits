<span>Critical-Kits: Art, technology and distributed participation Wednesday 30th November 2016 10am - 6pm</span>

[http://criticalkits.re-dock.org](https://www.google.com/url?q=http://criticalkits.re-dock.org)

### <span>Welcome to </span><span class="c5">Re-Dock,</span><span> the </span><span class="c5">Liverpool Small Cinema</span><span> and </span><span class="c5">Critical-Kits</span> 

<span class="c6"></span>

<span>As we’ve already said, as part of a generation of artists giving
equal critical focus to both participation and technology, we think
we’ve identified a problem to do with documentation. Much of the
documentation we and commissioning entities use often fails to capture
the richness, complexity and difficulty of working across contemporary
art practice, community engagement and technical cultures</span>

<span></span>

<span>We’ve already sent you some initial questions that we may touch
on. </span>

<span></span>

<span>A final recap on terms; we are using the term </span><span
class="c6">critical-kits</span><span> as a shorthand for issues around
documenting and sharing participatory artistic practice that uses
technology not </span><span class="c5">just</span><span> projects that
</span><span class="c5">use</span><span> kits. We will refer to not just
specific technologies (eg. webapps, microcontrollers, IoT, digital
fabrication) but also broader </span><span
class="c5 c6">technical</span><span
class="c5"> </span><span>and</span><span class="c5"> </span><span
class="c5 c6">participatory cultures.</span><span> We are stretching the
definitions a little.</span>

### <span>Aim</span>

<span>Through this symposium we’re trying to understand how we can use
</span><span class="c6">‘critical-kits’</span><span> and kit
cultures</span><span class="c6"> </span><span>to better communicate the
richness and complexity of art that engages with both participation and
technology.</span>

### <span>Observations</span>

<span></span>

<span>Responding to our observations of recent impulses for artists to
make ‘kits’, we roughly outlined  3 basic motivations:</span>

<span></span>

-   <span>To better distribute your practice without you </span><span
    class="c5">being there</span>
-   <span>To explore new kinds of art experiences and practice</span>
-   <span>To make money and be sustainable</span>

<span></span>

<span>What we are doing, is making connections between these impulses
and motivations, to some of the problems of documentation in
participatory art that uses technology.</span>

<span class="c6"></span>

### <span></span>

### <span>Opening Statements</span>

<span class="c6"></span>

<span class="c6">Aspects of ‘kit culture’ offer problems and
opportunities that are relevant to the challenge of documenting
participatory art. </span>

<span class="c6"></span>

<span class="c6">Document more but filter more: How do we do it, why,
when and to who?</span>

<span class="c6"></span>

<span>By responding to kit cultures, what we are really are trying to
focus on is the nature of the   documentation of artists and others’
participatory work with technology. We’ve already sent you a list of
questions, some of which are really far reaching and we’ll touch on many
of them, but will always be pulling back to it’s implications in terms
of documentation. </span>

<span></span>

<span>We are being open as to what this ‘documentation’ could be; We’ve
referred to </span><span class="c6">git </span><span>a </span><span
class="c5">version control</span><span> and project management system
from technical/engineering culture</span><span> quite a bit, but only as
an example representative of just how much fine grain of detail can be
‘tracked’ and documented. We are by no means suggesting it as a solution
to everything but a model of a way of working taken from technical
culture.</span>

<span></span>

<span>There are other forms of documentation of course that we want
people to discuss: hand bound books, board games, videos of
choreography, blogs, games or text adventures.</span>

<span></span>

<span class="c6">Key Questions / Prompts</span>

<span></span>

-   <span>What are the artistic aims of your kit or practice?</span>
-   <span>Constrain the intended audience and use scenario for your kit
    using Who? What? Where? When? How? </span>
-   <span>What aspects of the process of our participatory arts practice
    are we not documenting?</span>

<!-- -->

-   <span>Artistic and/or critical contexts</span>

<!-- -->

-   <span>Artistic aims, What are you trying to do?</span>
-   <span>Motivations?</span>
-   <span>Information about the use context.</span>

<!-- -->

-   <span>Implementation: Doing it</span>

<!-- -->

-   <span>Facilitation notes, past skills and plans</span>
-   <span>Learning Objectives if there are any (they don’t have
    to be)</span>
-   <span>Emergency facilitation skills; your critical framework/arduino
    won’t work in the sports hall: what do you do? </span>
-   <span>Planning & logistical info</span>

<!-- -->

-   <span>Reflection</span>

<!-- -->

-   <span>Documentation of how the kit has been used</span>
-   <span>Post mortem and de-brief issue list/blog</span>

<!-- -->

-   <span>How does your kit relate or use other forms of publication and
    distribution of art?</span>

<span></span>

### <span>Commentary</span>

<span></span>

<span>‘More data’ is not always useful: once we </span><span
class="c5">have everything missing documented,
</span><span>w</span><span>hat opportunities are there for filtering
this into a clearer more nuanced, fine grained, picture of our practice?
The artist’s role is to be that filter for all this and perhaps this is
where </span><span class="c5">authorship </span><span>resides. </span>

<span></span>

<span>Another idea that is often implicit in Maker Culture is that kits
can and should be for Everyone. While this promise of unlimited
distribution is appealing, especially for social and educational
projects, this could have a standardising effect on tone and content of
artist kits. So while it’s important that there are art kits that are
accessible to e.g. under 10’s, not all our kits have to be created with
them in mind.</span>

<span></span>

### <span>FInal </span><span>Outcomes post Symposium</span>

<span></span>

<span>Whatever we discover we will attempt to document it and then point
it toward partly answering the following: </span>

<span></span>

<span class="c5 c6">How can we best communicate this kind of art
practice to the art community and to wider audiences and the public
sphere?</span>

<span class="c6"></span>

### <span>Documentation </span> 

<span class="c6"></span>

<span>Aha! What we’ve been talking about. Here’s a strategy to start
with:</span>

-   <span>Filmmaker and artist Tim Brunsden of Re-Dock will livestream
    film elements of the kit-crits.</span>
-   <span>Ross will add gitlab issues with the label ‘</span><span
    class="c6">references’</span><span> and pinboard bookmarks with the
    </span><span
    class="c6">criticalkits</span><span> tag</span><span> throughout the
    day</span>
-   <span>We will be using the twitter Hashtag </span><span
    class="c6">\#CriticalKits</span><span> for the event but we’d also
    like to experiment with the idea of using an additional tag
    </span><span class="c6">\#on</span><span> during our discussions
    which you use to indicate someone has said something important
    during a discussion you want to refer to/respond to or document. It
    refers to and prototypes our idea of </span><span class="c6">A magic
    button to tag a video stream live</span>
-   <span class="c6">A magic button to tag a video stream live:</span>

<span>Make a button maybe with node-red to tag a video stream when
someone says something interesting or needs further comment / research /
data / response / backing up / confirming / abuse / laughter/ make
another conference about it</span>

<span></span>

<span>You can access Ross & Neils pinboard links, issues and the twitter
stream from the </span><span class="c6">References tab </span><span>from
the symposium webpage </span><span
class="c11">[http://criticalkits.re-dock.org](https://www.google.com/url?q=http://criticalkits.re-dock.org&sa=D&ust=1480437106424000&usg=AFQjCNHW5hlAQo-RcAIK6tyNQkqyp7pCxw){.c12}</span>

<span class="c6"></span>
