## Artist kit Antecedents Top Trumps

<img src="top_trump_png/albers_paper_kits.png" width="800">

Prototype tools for examining kit cultures through art history and the medium of Top Trumps.

Use template, Print out, cut out fold and compete with friends for fun and art as utility knowledge transfer.

Optional warm up activity: [Maker Mandems](../MakerMandems.md) 



