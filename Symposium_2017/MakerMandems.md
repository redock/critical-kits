
# Maker Mandems / Gyaldems

We will be doing a brief slightly juvenile group mapping activity to work out our relationship to technology culture. 

Let's admit that a *tiny* part of the urge for artists to 'make kits' or adopt technology into their work, is to be part of a gang you respect; a collective, a community: something chimes with an idea you have or represents your point of view in the wider political sphere: You think it's punk-rock/acid-house/a soul classic etc.

## `gangs-of-kits`

Whats their MO as a gang? What are the minimum requirements?  Why do you want to join it or not? Invent some gangs!

### Tech Will Save Us Territorial Army

Everything is awesome! Technology will sort everything out for everybody all of the time. We will build and ship almost anything with hyperbole!

### Critical Engineering Crew

Everything is not awesome! It all represents the militarisation and exploitation of all human endeavour/ [They Live](https://www.youtube.com/watch?v=JI8AMRbqY6w) is a documentary. We're still going to build stuff though!

### Post Structuralism Posse

Everything underlying everything is not awesome! We are unlikely to build anything.

### Fablab Fam

We are going to build everything! Manufacturing is something we invented and we're going to do it with our business model!

### Linux Goths

Everything in the kernel is awesome! If only people would document everything and stop trying to hide what they are doing then maybe we can be more awesome.

### Standards Committees

Hardcore. Everything is awesome if we just have some standards.

### PhD Party

Research is awesome! We need to check out all this DIY Fabby code-club Maker stuff because they are making awesome stuff/transferring knowledge without a methodology!

### Open Hardware Headcases

The stuff we build with and on can be shared and copied. That's awesome.

### Do our own thing

Fair enough that's awesome. Technology has been around for ages. Do some drawing / coding / play outside and with others.

### Die Hard Dilettants

You are fascinated by the different ways all these gangs work and try a bit of everything. Despite it's negative connotations it can be a healthy approach but it's nicer in a community sometimes. 

### Wu-LAN Clan

We dont know anything about them but it's the name of a WiFi network at [DoESLiverpool](http://doesliverpool.com) I dont think Method(ology) Man is still in them.
