## Critical-Kits Symposium schedule

Time | Activity  
------- | ----------- 
09.45  | Arrival teas/coffees
10:00  | Brief intro Plant Synth Kits by Laura Pullig & Lasercut Pinhole Camera kits by Chris Huffee
10.45  | Presentation: 100 years of artists kits & GYM JAMS by Re-Dock
11:00  | [Maker ManDems] (https://gitlab.com/redock/critical-kits/blob/master/MakerMandems.md) / Group mapping activity
11:30  | Crit 1A
11:30  | Crit 1B
12:30  | Discussion 1A & 1B
13:00  | Whole group feedback
13:15  | Lunch
13:45  | Presentation: The dark matter of communal documentation
14:00  | Crit 2A
14:00  | Crit 2B
15:00  | Discussion 2A & 2B
15:30  | Group feedback
15:45  | Coffee & Cake
16:00  | Writing for kits exercise
16:20  | Push back to your own practice
17:30  | Reflection/Documentation
18:00  | Pub

