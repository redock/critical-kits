# Critical Kits: Art, technology and distributed participation

## Wednesday 30th November 2016 10am - 6pm

<a href="http://www.wtfpl.net/"><img src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png" width="80" height="15" alt="WTFPL" /></a>

## [Liverpool Small Cinema](http://liverpoolsmallcinema.org.uk/category/visit-us)

Portal, source files, information and useful code for the Critical Kits: Art, technology and distributed participation Symposium

<img src="http://re-dock.org/wp-content/uploads/2016/10/HowieB-and-OLO.jpg" width="400">

[Re-Dock](http://re-dock.org/) are organising a symposium on the **30th November 2016** about participatory art: in particular, looking at critical practice around participatory art that uses technology. The event will take place at **A Small Cinema Liverpool**.

As part of a generation of artists giving equal critical focus to both participation and technology, we think we’ve identified a problem. Much of the documentation of this work fails to capture the richness, complexity and difficulty of working across contemporary art practice, community engagement and technical cultures. 

We’d like to invite you to critically reflect with us on the opportunities in evolving participation, technical and network cultures for the better distribution of artistic practice. 

Normally much of the presentation of this work uses formats shaped by print and broadcast media, reports, documentary films and photographs. We want to explore how formats shaped by networked culture, such as github repos, instructional guides and maker kits could be used to communicate the social and cultural meaning of projects as well as their technical function. 

You can [read about some of the reasons this repository exists and why it's on gitlab here](https://gitlab.com/redock/critical-kits/blob/master/Why_Git.md).

