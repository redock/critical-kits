
### Why Git?

What is documentation in the contemporary world? We've been looking at some tools from technical culture like [git](https://git-scm.com/). We are not suggesting it's **the answer** to documentation issues but it's an interesting model in the way it can track and distribute information.

Also: **DON'T PANIC** you don't have to be on git to take part in the day!

Git is a *version control* system; if you are building complex bit of software or hardware with other people you need to keep track of what everyone is doing. You may make a mistake 2 months into making something and discover a tiny error in one persons code or how to text is ruining everything. Git tracks everything everyone does and would allow you to find the error and fix it or `git blame` the person responsible. It also makes it simple to open source something so that other people can `git clone` your work.

It also means anyone (or anyone you choose) can use your work and in some cases people could even *depend* on your work. That's an interesting place to be and it does sometimes require a degree of responsibility, but to counter that it can be very rewarding.

Of course it's meant for developing and distributing `code` and quite technical but as we all know artists are good at making tools do slightly different things:  

Ross AKA [@cheapjack](https://twitter.com/cheapjack) has been using git to cope with complicated community projects which he'll talk about after lunch and also as a way of documenting & distributing his and others work

[GitHub](https://github.com) is the most well known home of git and its as pervasive as google so pretty much everyone is using it. We are using [GitLab](https://gitlab.com) as it's like [GitHub](https://github.com) but with options for hosting it yourself, something [Re-Dock](http://re-dock.org/) may do in the future. It's also got some snazzy features that Github doesn't and just feels a bit more independent. We've organised this symposium almost completely using Google docs
so think we wanted a tiny bit of potential freedom from our digital overlords.

If you are interested the guides below should help you get started, if you already use github just [sign up to Gitlab from here](https://about.gitlab.com/) and login with your existing github account. This may be the best thing to do as GitHub is such a bigger community.

You can then easily contribute to our [issue list](https://gitlab.com/redock/critical-kits/issues) without doing any actual `git` stuff and is a great way to take part in group discussions etc. If you want to edit and add files you can make a [fork](https://help.github.com/articles/fork-a-repo/) of the project, into your own gitlab account and add a new branch which you can then make a pull request to include it in the original.

This all starts to get really complicated and scary so you should refer to the guides below really. I really recommend starting with the [no deep shit](https://rogerdudler.github.io/git-guide/) guide and then getting more detail from [Scott Lowe's Excellent Workflow Guide](http://blog.scottlowe.org/2015/01/27/using-fork-branch-git-workflow/).

In terms of documenting your thinking or notes at our symposium you *could* setup & sign-in with a gitlab account, find our repo location and click the **fork** button that pulls it into your account. It doesn't copy or clone it to your computer or anything but it means you could add some instantly readable `USEFUL_FILES.md` just using the gitlab browser. Anything ending in `.md` like `README.md` is rendered as text using Markdown which uses tags to make things **bold** or *italic* or a heading which you can find out about on this [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). If you do this gitlab makes a new branch of the project and everything gets saved in your gitlab account and you can make this as public as you want or even make a pull request if you think we should pull it into the original project repo the `master`.

Once you want to start cloning the project and really getting your hands dirty you can [install git](https://git-scm.com/) and work either using the command-line, a free or paid for [Application](https://git-scm.com/downloads/guis) or even a mobile [App](https://about.gitlab.com/applications/))

I started with a terrible fear of git (making confusing mistakes in public!) but found I just loved how you could use issues to project manage things and write and publish explanations, how-tos and answers to Frequently Asked Questions using `README.md` files just in the browser. For Desktop Prosthetics I didn't touch any of the code (I'm not a programmer) I just made useful `.md` files.

If you want to get more involved in the critical-kits repo you may have to <a href="mailto:ross@cheapjack.org.uk?Subject=Hello%20I%20want%20to%20collaborate%20on%20critical-kits">ask Ross to be a collaborator</a>

Once you're setup as a `reporter` you can add and edit issues & labels etc. There's a brief set of guidelines on [contributing here](https://gitlab.com/redock/critical-kits/blob/master/CONTRIBUTING.md)

You can clone the repo into your own computer on the command-line with `$ git clone https://gitlab.com/redock/critical-kits.git` and use any old text editor to make a README.md file just add the `.md` extension and when you push it to your own gitlab or others it's rendered as readable text.

If you have never used `git` it before, you may need to [install](https://git-scm.com/) it but `osx`, Raspbian and various flavours of Linux may already have it...

#### [GitLab Basic Getting Started on the Command Line](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html)

#### [GitLab Basics Overview](https://docs.gitlab.com/ce/gitlab-basics/README.html)

And the one I always refer to when I forget as it's super clear; there is no `deep shit`
#### [No Deep Sh*t Guide](https://rogerdudler.github.io/git-guide/)

#### Handy [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

Clone, contribute fork etc see you on the `gitosphere`
