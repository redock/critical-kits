# Critical Kits: Art, technology and distributed participation

<a href="http://www.wtfpl.net/"><img src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png" width="80" height="15" alt="WTFPL" /></a>

Portal, source files, information and useful code for the Critical Kits project

<img src="http://domesticscience.org.uk/data/c_k3.png" width="400">

[Re-Dock](http://re-dock.org/) organised a symposium on the **30th November 2016** about participatory art and making: in particular, looking at critical practice that uses technology. The event took take place at [A Small Cinema Liverpool](http://www.liverpool.asmallcinema.org/).

As part of a generation of artists giving critical focus to both participation and technology, we think we’ve identified some problems. When first thinking on kits after noticing a surge of them in technology education and art we noticed that much of the articulation, distribution and documentation of this sort of work using conventional media, fails to capture the richness, complexity and difficulty of working across contemporary art practice, the so-called Maker movement, community engagement and wider technical cultures. Kits with their mobility and convenience often feature elaborate instructional materials of mixed media that potentially represent these practices better. At the same time they distribute the practice or project itself allowing others to participate further. Their utility and mobility and need for maintenance make them more like living archives over simple reporting after the fact. This is part of a kit's appeal particularly to artists and makers.

Documentation aside, Kits come in many forms and are highly useful. They can be instructionals, tools, artistic and political speculation, component packages, software, hardware, sports cars, help notifications, humanities 'toolkits', products, survival packs; things packaged up to help us explore, play, build and maintain. Mobile & convenient they help artists, scientists, makers, engineers, theorists, planners and maintainers intervene & share in the world. But this convenience hides a problem: by abstracting or simplifying complicated components or concepts into kits for ease of use they may remove opportunities for deeper nuanced experiences of understanding and learning. When used to fix a puncture or implement a feature in software this can be overlooked, but when used to understand fundamental concepts or contexts in art, science or address concerns or problems in a community they potentially become a major problem. Kits and by extension, other modular ‘solutions’ to knowing & doing, can make us feel like we are in control, literate and resilient when we are not.

*Critical kits*, however, maintain well-crafted convenience but explore more open and contextual spaces for discovery, learning and doing.

Critical kits become a useful frame for critically exploring participation, technical and network cultures for the better distribution of artistic and making practices. 

## Publication  

### "Critical Kits and How We Use Them"

A collaboration between [Torque](http://torquetorque.net/) and [Re-Dock](http://re-dock.org/) Critical Kits includes a selection of case studies from artists and makers working in the kit form, a series of essays on the theory, historical and contemporary contexts for kit making and distribution, and an in depth look Gym Jams, a kit-based project which took place in a public leisure centre. It was written and edited by Re-Dock, Nathan Jones and artists from across the north of England working creatively with technology and people.

You can [download the pdf here](publication/Critical-Kits-And-How-We-Use-Them.pdf) or buy it alongwith the Critical Kit Trump cards game [here at the Re-Dock shop](https://shop.re-dock.org/)

The book is about DIY 'maker' culture and how it meets participatory and community-based forms of creative practice. Critical kits are toolboxes, resources, instructions for how to make great, or simply interesting, things happen with technology. But they also ask that we be aware of the network of effects technologies participate in. It is designed to be useful for artists, makers, students of art-tech work, and anyone interested in current
participatory and technology practices.

## Social Media

[@CriticalKits](https://twitter.com/CriticalKits) on twitter will be a feed for things we think act like critical kits or benefit from critical kit thinking

## Further Research

Ross Dalziel AKA [@cheapjack](https://twitter.com/cheapjack) is taking elements of the project into his PhD research with Lancaster Institute of Contemporary Arts (LICA) supported by a Lancaster University [Faculty of Arts and Social Sciences (FASS)](http://www.lancaster.ac.uk/arts-and-social-sciences/) scholarship. Looking at agency and resilience in bioscience and puts forward a role for Critical Kits in science and technology studies. It extends Critical Kits as a methodology in the context of bio-technology and post-maker culture to improve scientific literacy, agency and resilience through Ross' participatory and socially engaged art practice.

His supervisory team led by artist [Jen Southern](http://www.theportable.tv/) includes [Dr Rod Dillon](http://www.lancaster.ac.uk/fhm/about-us/people/rod-dillon) at the Biomedical and Life Sciences at Lancaster University, working on aspects of communication, chemo-taxis and movement in biological systems and builds on Rod’s ongoing work embedding artistic practices in research and teaching. It will also work with a sub-set of the Wearable technology interest group at makerspace [DoESLiverpool](http://doesliverpool.com) re-igniting DIY Bio type practices in the Northwest.

## Education

Crtitical Kits could be seen as a response to the rise of STEAM approaches to participatory art, making and technology. STEAM makes little articulation on what the 'A' in the acronym stands for; 'Art' in STEM teaching can sometimes just stand for a general 'creativity' in the approach to STEM subjects. The Critical Kit perspective is that the A could stand for *art practices*. Rod Dillon although superficially getting microbiology lab practice students to draw things with bacteria and agar, actually developed a module on contemporary art practices that approached biosciences. This is a much more sophisticated approach than just using Art to make things easily engaging or playful. It has to be said it's worth not discounting the playful value of art and we don't want critical kits to seem over serious and non playful, if anything we want to value serious play.

RE-Dock co-director Neil Winterburn AKA [@onthepennines](https://twitter.com/onthepennines) as Educator at [FACT](http://fact.co.uk) will be working with Ross & Lucia Arias on a pilot education programme responding to a collaboration with CERN. 


## Documentation

Normally much of the presentation of this work uses formats shaped by print and broadcast media, reports, documentary films and photographs. We want to explore how formats shaped by networked culture, such as github repos, instructional guides and maker kits could be used to communicate the social and cultural meaning of projects as well as their technical function. 

You can [read about some of the reasons this repository exists and why it's on gitlab here](https://gitlab.com/redock/critical-kits/blob/master/Why_Git.md).

We'll also be adding [documentation issues](https://gitlab.com/redock/critical-kits/issues?label_name%5B%5D=documentation-ideas) tagged with `documentation-ideas` and some links on a critical kit [documentation pinboard](https://pinboard.in/u:cheapjack/t:criticalkits/t:documentation-ideas)

