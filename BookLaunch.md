# CRITICAL KITS AND HOW WE USE THEM
## Booklaunch
## Wednesday 29th November 6pm
### FACT, The Learning Space, FACT, 88 Wood Street, Liverpool L1 4DQ

## CRITICAL KITS AND HOW WE USE THEM
creative commons licence 2017
designed by Hwa Young Jung
Edited by Nathan Jones and Sam Skinner
Published in Liverpool by <a href="http://www.torquetorque.net">Torque Editions</a>
ISBN 978-09932487-5-7 

[CRITICAL KITS WEBSITE](http://criticalkits.re-dock.org/)

[TICKETS](https://www.eventbrite.co.uk/e/critical-kits-and-how-we-use-them-book-launch-tickets-39452217645)

### Introduction

Re-Dock and Torque publications invite you to the launch of a new book ‘Critical Kits and How We Use Them’ on Wednesday 29th November 2017 from 6pm in  The Learning Space at FACT, 88 Wood Street Liverpool L1 4DQ. We invite you to join us to leaf through the book, get hands on with some kits and play the card game!

‘This book is about DIY culture and how it meets participatory, inclusive and community-based forms of creative digital practice.

We see Critical kits as toolboxes, resources, or instructions for making and distributing creative work using technology. By ‘Critical’ we mean an expectation to question and be aware of the wider implications of working in this way.

Critical Kits includes a selection of case studies from artists and makers working in the kit form, a series of essays on the theory, historical and contemporary contexts for kit making and distribution, and an in depth look Gym Jams, a kit-based project which took place in a public leisure centre.

It is designed to be useful for artists, makers, students of art-tech related work and anyone interested in current participatory and technology practices.’ 

The launch is an informal social to thank all the contributors and to introduce the book to a wider group of people and to reflect on what we've written and thought about. It's also a chance to catch up on what we've all been up to and start thinking about where the Critical-Kits project can go next and play a few rounds of Critical-Kits Trumps!

### FAQ
#### What is this book about?
The book investigates the relationship between art, maker & technical cultures through the emergence of kits as a form of creative distribution and publication through a selection of artists writing, a portfolio of artist made kits and the unpacking of various kit based art projects like GYMJAMS which took place in a leisure centre.

#### Who are Re-Dock?                                     
We are an artist collective whose work grows out of the intersection of kit making culture and public art. We make participatory art in non gallery spaces (libraries, leisure centres, shopping centres, canal towpaths) that explore people’s relationship to emerging technologies. Although our projects are initiated by artists in response to specific places and communities, we have found ways to reconstruct them with different communities in different places. This process has led us to view some aspects of participatory art practice as a kind of a kit. 

#### Why have you written this book?
As part of a generation of artists giving equal critical focus to both participation and technology, we think we’ve identified a problem. Much of the documentation of this work fails to capture the richness, complexity and difficulty of working across contemporary art practice, community engagement and technical cultures. This written exploration of the artistic uses of kits and kit culture is part of our response to this challenge.
